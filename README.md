# VicPAH Package Registry
This project contains the VicPAH package registry that other project CI uses to publish to.

There is no code here; the only thing this contains is in the packages & registries feature of GitLab.